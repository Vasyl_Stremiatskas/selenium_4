import BusinesLogic.BusinessLogic;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestGmail {

    @Test(dataProvider = "user-data")
    public void gmailLoginSendDeleteTest(String[] info) {
        BusinessLogic logic = new BusinessLogic();
        logic.login(info[0], info[1]);
        //Assert.assertTrue(logic.isLoginSuccess());
        logic.sendMessage(info[2], info[3], info[4]);
        Assert.assertTrue(logic.isMsgSend());
        logic.deleteSentMail();
        //Assert.assertFalse(logic.isMsgDeleted());
    }

    @DataProvider(name = "user-data", parallel = true)
    public Object[][] provide() throws Exception {
        String[][] info = new String[5][5];
        info[0][0] = "selenium111111111111111@gmail.com";
        info[0][1] = "1111test";
        info[1][0] = "selenium11111111111111@gmail.com";
        info[1][1] = "1111test";
        info[2][0] = "selenium1111111111111@gmail.com";
        info[2][1] = "1111test";
        info[3][0] = "selenium111111111111@gmail.com";
        info[3][1] = "1111test";
        info[4][0] = "selenium11111111111@gmail.com";
        info[4][1] = "1111test";


        for(int i = 0; i < 5; i++) {
            info[i][2] = "vasua.vasua1999@gmail.com";
            info[i][3] = "selenium";
            info[i][4] = "text";
        }
        return info;
    }


}
