package BusinesLogic;

import PageObjects.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BusinessLogic {
    private WebDriver driver;
    private boolean isLoginSuccess;
    private boolean isMsgSend;
    private  boolean isMsgDeleted;

    private WebDriver startup() {

        WebDriver driver;
        System.setProperty("webdriver.chrome.driver", "\\JavaProjects\\Selenium_2.0\\WebDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/intl/ru/gmail/about/");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        return driver;
    }

    public BusinessLogic() {
        this.driver = startup();
    }

    public void login(String userLogin, String userPassword) {
        GmailMain gmailMain = new GmailMain(driver);
        isLoginSuccess = gmailMain.isGmailMainPage();
        LoginPage loginPage = gmailMain.clickLogin();
        isLoginSuccess = loginPage.isLoginPage();
        PasswordPage passwordPage = loginPage.sendLogin(userLogin);
        isLoginSuccess = passwordPage.isPasswordPage();
        passwordPage.sendPassword(userPassword);
    }

    public void sendMessage(String to, String subject, String text) {
        InboxMails mailsPage = new InboxMails(driver);
        isMsgSend = mailsPage.isInboxMailsPage();
        MailForm mailForm = mailsPage.sendMailBtn();
        isMsgSend = mailForm.isMailForm();
        mailForm.sendMessage(to, subject, text);
    }

    public void deleteSentMail() {
        InboxMails inboxMails = new InboxMails(driver);
        SentMail sentMails = inboxMails.goToSentMails();
        isMsgSend = sentMails.isSentMailPage();
        sentMails.deleteMsg();
        isMsgDeleted = sentMails.isMsgDeleted();
    }

    public boolean isMsgDeleted() {
        return isMsgDeleted;
    }

    public boolean isLoginSuccess() {
        return isLoginSuccess;
    }

    public boolean isMsgSend() {
        return isMsgSend;
    }
}
