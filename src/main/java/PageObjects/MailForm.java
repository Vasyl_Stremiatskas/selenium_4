package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MailForm {
    private WebDriver driver;

    @FindBy(name = "to")
    private WebElement toField;

    @FindBy(name = "subjectbox")
    private WebElement subjectField;

    @FindBy(xpath = "//*[@id=\":65\"]")
    private WebElement textField;

    @FindBy(xpath = "//*[@id=\":7k\"]")
    private WebElement sendBtn;

    private boolean isMailForm = true;

    public MailForm(WebDriver driver) {
        this.driver = driver;
        try {
            PageFactory.initElements(driver, this);
        } catch (Exception ex) {
            isMailForm = false;
        }
    }


    public void sendMessage(String to, String subject, String text) {
        toField.sendKeys(to);
        subjectField.sendKeys(subject);
        textField.sendKeys(text);
        sendBtn.click();
    }

    public boolean isMailForm() {
        return isMailForm;
    }
}
