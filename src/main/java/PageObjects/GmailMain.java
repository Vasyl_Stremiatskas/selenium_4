package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class GmailMain {
    private WebDriver driver;

    @FindBy(xpath = "/html/body/nav/div/a[2]")
    private WebElement loginBtn;

    private boolean isGmailMainPage = true;

    public GmailMain(WebDriver driver) {
        this.driver = driver;
        try {
            PageFactory.initElements(driver, this);
        } catch (Exception ex) {
            isGmailMainPage = false;
        }
    }

    public LoginPage clickLogin() {
        loginBtn.click();
        LoginPage loginPage = new LoginPage(driver);
        return loginPage;
    }

    public boolean isGmailMainPage() {
        return isGmailMainPage;
    }



}
