package PageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"identifierId\"]")
    private WebElement loginField;

    private Boolean isLoginPage = true;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        try {
            PageFactory.initElements(driver, this);
        } catch (Exception ex) {
            isLoginPage = false;
        }
    }


    public PasswordPage sendLogin(String login) {
        loginField.sendKeys(login);
        loginField.sendKeys(Keys.ENTER);

        PasswordPage passwordPage = new PasswordPage(driver);

        return passwordPage;
    }

    public boolean isLoginPage() {
        return isLoginPage;
    }
}
