package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SentMail {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\":4\"]/div[3]/div[1]/div/div[2]/div[3]")
    WebElement deleteBtn;

    private boolean isSentMailPage = true;
    private boolean isMsgDeleted = true;

    public SentMail(WebDriver driver) {
        this.driver = driver;
        try {
            PageFactory.initElements(driver, this);
        } catch (Exception ex) {
            isSentMailPage = false;
        }
    }

    public void deleteMsg() {
        deleteBtn.click();
        WebElement element;
        try {
            element = driver.findElement(By.xpath("//*[@id=\"link_undo\"]"));
        } catch (Exception ex) {
            isMsgDeleted = false;
        }
    }

    public  boolean isSentMailPage() {
        return isSentMailPage;
    }

    public boolean isMsgDeleted() {
        return isMsgDeleted;
    }
}
