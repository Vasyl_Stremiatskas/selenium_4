package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class InboxMails {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\":3t\"]/div/div")
    private WebElement sendMailBtn;

    @FindBy(xpath = "//*[@id=\"link_vsm\"]")
    private WebElement sentMails;

    private boolean isInboxMailsPage = true;


    public InboxMails(WebDriver driver) {
        this.driver = driver;
        try {
            PageFactory.initElements(driver, this);
        } catch (Exception ex) {
            isInboxMailsPage = false;
        }
    }

    public MailForm sendMailBtn() {
        sendMailBtn.click();
        MailForm mailForm = new MailForm(driver);
        return mailForm;
    }

    public SentMail goToSentMails() {
        sentMails.click();
        SentMail sentMail = new SentMail(driver);
        return  sentMail;
    }

    public boolean isInboxMailsPage() {
        return isInboxMailsPage;
    }
}


