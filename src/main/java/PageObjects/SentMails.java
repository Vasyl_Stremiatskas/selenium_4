package PageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;


public class SentMails {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\":np\"]/span")
    private WebElement toField;

    @FindBy(xpath = "//*[@id=\":nm\"]/span")
    private WebElement subjectField;

    @FindBy(xpath = "//*[@id=\\\":no\\\"]/div/div/span\"")
    private WebElement textField;

    @FindBy(xpath = "//*[@id=\":4\"]/div[2]/div[1]/div[1]/div/div/div[2]/div[3]/div/div")
    private WebElement deleteBtn;

    SentMails(WebDriver driver) {
        this.driver = driver;
    }




    public boolean compare(String to, String subject, String text) {
        if(to.equals(toField.getText()) && subject.equals(subjectField.getText()) && text.equals(textField.getText())) {
            return true;
        }
        else {
            return false;
        }
    }

    private void checkLastMessage() {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("document.getElementById(':ns').setAttribute('checked','true')");
    }

    public void deleteLastMail() {
        checkLastMessage();
        deleteBtn.click();
    }


}
