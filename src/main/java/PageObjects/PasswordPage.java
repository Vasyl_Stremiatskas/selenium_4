package PageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PasswordPage {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordField;

    private boolean isPasswordPage = true;

    public PasswordPage(WebDriver driver) {
        this.driver = driver;
        try {
            PageFactory.initElements(driver, this);
        } catch (Exception ex) {
            isPasswordPage = false;
        }
    }


    public InboxMails sendPassword(String password) {
        passwordField.sendKeys(password);
        passwordField.sendKeys(Keys.ENTER);
        InboxMails maailsPage = new InboxMails(driver);
        return maailsPage;
    }

    public boolean isPasswordPage() {
        return isPasswordPage;
    }
}
