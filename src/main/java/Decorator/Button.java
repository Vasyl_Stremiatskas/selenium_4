package Decorator;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Button extends AbstractDecorator {
    private WebElement element;

    public Button(WebElement element) {
        this.element = element;
    }

    @Override
    public void click() {
        element.click();
    }

}
