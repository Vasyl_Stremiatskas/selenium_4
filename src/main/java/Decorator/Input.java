package Decorator;

import org.openqa.selenium.WebElement;

public class Input extends AbstractDecorator {
    private WebElement element;

    public Input(WebElement element) {
        this.element = element;
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        element.clear();
        element.sendKeys(charSequences);
    }


}
